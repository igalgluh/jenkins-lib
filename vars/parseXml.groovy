@NonCPS
def call(){
      def toEncode = "dehydrogenase (NADP+)"
    println java.net.URLEncoder.encode(toEncode, "UTF-8") //== "dehydrogenase+%28NADP%2B%29"
    def xmlString = '''
    <testsuites status="Failure" count="5" duration="10">
        <testsuite status="Failure">
            <testcase>1 Simple example of xml file</testcase>
        </testsuite>
        <testsuite status="Failure">
            <testcase>2 Simple example of xml file</testcase>
        </testsuite>
    </testsuites>'''
   // sh "echo xmlString > test_result.xml"
    //def file = new File('test_result.xml') 
   //def xml = new XmlParser.parseText(file)
    //println xml.@status
     def testsuites = new XmlSlurper().parseText(xmlString) 
     
     println testsuites.testsuite[0].testcase
     println testsuites.testsuite[1].testcase
     println "status:" + testsuites.@status
     println "duration:" + testsuites.@duration
     testsuites.testsuite.each { testsuite ->
        println testsuite.testcase
     }
     
     
     def parsed = new XmlSlurper().parseText(xmlString)
//Create the map as needed out of parsed xml
def map = [(parsed[0].name): parsed.'**'
  .findAll{it.name() == 'row'}
  .collect{ row ->
     row.collectEntries{[KEY: row.id.text(), VALUE:row.value.text()]}
   }
]
println new groovy.json.JsonBuilder(map).toPrettyString()
     
     
     
     
     
}